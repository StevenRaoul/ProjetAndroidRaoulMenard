package spam.spam;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "score_table")
public class Score {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "pseudo")
    private String pseudo;
    private int points;

    public Score(String pseudo, int points) {
        this.pseudo = pseudo;
        this.points = points;
    }

    public String getPseudo(){return this.pseudo;}
    public int getPoints(){return this.points;}
    public String  getScore(){return (this.pseudo+": "+this.points);}
}

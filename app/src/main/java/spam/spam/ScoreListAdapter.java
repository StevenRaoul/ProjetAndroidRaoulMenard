package spam.spam;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by raoul on 04/04/18.
 */

public class ScoreListAdapter extends RecyclerView.Adapter<ScoreListAdapter.WordViewHolder> {

    class WordViewHolder extends RecyclerView.ViewHolder {
        private final TextView wordItemView;

        private WordViewHolder(View itemView) {
            super(itemView);
            wordItemView = itemView.findViewById(R.id.textView);
        }
    }

    private final LayoutInflater mInflater;
    private List<Score> mScore; // Cached copy of words

    ScoreListAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @Override
    public WordViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new WordViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(WordViewHolder holder, int position) {
        if (mScore != null) {
            Score current = mScore.get(position);
            holder.wordItemView.setText(current.getScore());
        } else {
            // Covers the case of data not being ready yet.
            holder.wordItemView.setText("Aucun record");
        }
    }

    void setWords(List<Score> score){
        mScore = score;
    }

    // getItemCount() is called many times, and when it is first called,
    // mWords has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mScore != null)
            return mScore.size();
        else return 0;
    }
}
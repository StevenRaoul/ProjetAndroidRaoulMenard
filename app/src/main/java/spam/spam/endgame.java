package spam.spam;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class endgame extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        final int score = intent.getIntExtra("score", 0);
        setContentView(R.layout.endgame);
        TextView scoreText = (TextView) findViewById(R.id.scoreFin);
        scoreText.setText(score + "");
    }




    public void next(View v){

        Intent intent = new Intent(endgame.this, SpamMenu.class);
        startActivity(intent);
    }
}




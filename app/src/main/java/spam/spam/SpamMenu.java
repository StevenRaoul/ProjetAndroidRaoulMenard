package spam.spam;

import android.content.Intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;



public class SpamMenu extends AppCompatActivity {

//    private ScoreViewModel mWordViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        RecyclerView recyclerView = findViewById(R.id.recyclerview);
//        final ScoreListAdapter adapter = new ScoreListAdapter(this);
//        recyclerView.setAdapter(adapter);
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
//        mWordViewModel = ViewModelProviders.of(this).get(ScoreViewModel.class);
        setContentView(R.layout.activity_spam_menu);

        final Button playButton = (Button) findViewById(R.id.jouer);
        playButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SpamMenu.this, activity_game.class);
                startActivity(intent);
            }
        });

        final Button scoreButton = (Button) findViewById(R.id.classement);
        scoreButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SpamMenu.this, Liste_score.class);
                startActivity(intent);
            }
        });
    }


}



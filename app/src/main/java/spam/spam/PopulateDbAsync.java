package spam.spam;

import android.os.AsyncTask;

/**
 * Created by raoul on 04/04/18.
 */

class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

    private final ScoreDao mDao;

    PopulateDbAsync(ScoreRoomDatabase db) {
        mDao = db.scoreDao();
    }

    @Override
    protected Void doInBackground(final Void... params) {
        mDao.deleteAll();
        Score score = new Score("Gabidoche", 123);
        mDao.insert(score);
        score = new Score("Stevia", 128);
        mDao.insert(score);
        return null;
    }
}

package spam.spam;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

/**
 * Created by raoul on 04/04/18.
 */

public class ScoreViewModel extends AndroidViewModel {

    private ScoreRepository mRepository;

    private LiveData<List<Score>> mAllScore;

    public ScoreViewModel (Application application) {
        super(application);
        mRepository = new ScoreRepository(application);
        mAllScore = mRepository.getAllScore();
    }

    LiveData<List<Score>> getAllScore() { return mAllScore; }

    public void insert(Score score) { mRepository.insert(score); }
}